﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 山札のView
public class DeckView : MonoBehaviour
{
    Text label;

    // Unityのコンポーネントの初期化は「Start」と「Awake」メソッドがある。
    // どちらもコンポーネントが生成されてすぐに1度だけ実行されるが、Awakeが
    // 必ず先に実行される。Awakeを使うことによって、他のコンポーネントのStart
    // よりも早く初期化が行われることを保証する。
    void Awake()
    {
        label = GetComponentInChildren<Text>();
    }
    
    // カードの数を指定する
    public void SetCount(int count)
    {
        label.text = count.ToString();
    }
}
