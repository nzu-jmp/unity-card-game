﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 手札のView
public class HandView : MonoBehaviour
{
    // 中身を空にする
    public void Clear()
    {
        // Unityでオブジェクトの「子」を一つずつ処理する場合、
        // foreachループでtransformをイテレーションする
        foreach(Transform child in transform)
        {
            // ゲームオブジェクトを消す
            Destroy(child.gameObject);
        }
    }
}
