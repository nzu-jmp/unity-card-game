﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardGame; // 自分がGame.csなどで定義したnamespaceを利用する

public class Controller : MonoBehaviour
{
    // AIプレーヤーへの参照
    public PlayerAI player2;

    // Viewへの参照
    public DeckView deckView;
    public ResultsView resultsView;
    public ScoreView[] scoreViews = new ScoreView[2]; // 2人分
    public HandView[] handViews = new HandView[2];　// 2人分
    public HandView[] selectionViews = new HandView[2];　// 2人分

    // 手札のカードを生成するためのプレハブ
    public GameObject cardPrefab;

    // モデル
    Game game;

    // Start is called before the first frame update
    void Start()
    {
        // コントローラが新しいモデルを生成する
        game = new Game();

        // ここで匿名関数を使って、モデルのイベントに反応する
        game.OnGameChange += () => {
            // UIを更新する...

            // 山札:
            deckView.SetCount(game.deck.Count);

            // 各プレーヤーごと...
            for (int p = 0; p < game.players.Count; p++)
            {
                // スコア表示:
                scoreViews[p].SetValue(game.players[p].score);

                // 手札:
                handViews[p].Clear();

                // 各手札のカードごと...
                for (int c = 0; c < game.players[p].tefuda.Count; c++)
                {
                    // カード情報を取得する
                    Card card = game.players[p].tefuda[c];
                    // 新しいカードViewをプレハブから生成する
                    CardView cardView = Instantiate(cardPrefab).GetComponent<CardView>();
                    // Viewを初期化する
                    cardView.Init(card.suit, card.value, p, c);
                    // カードViewを手札Viewの子にする
                    cardView.transform.SetParent(handViews[p].transform, false);

                    if (p == 0)
                    {
                        // P1だけがカードを選べられる
                        cardView.OnSelect += game.SelectCard;
                    }
                    else
                    {
                        // P2だけはカードを伏せる
                        cardView.SetHidden(true);
                    }
                    
                }

                // 選んだカード
                selectionViews[p].Clear();
                // プレーヤーが選んだカードを取得する
                Card selection = game.players[p].selection;
                if (selection != null) // ない可能性もある！
                {
                    // ここは手札と同じ方法で表示する
                    CardView cardView = Instantiate(cardPrefab).GetComponent<CardView>();
                    cardView.Init(selection.suit, selection.value, p, 0);
                    cardView.transform.SetParent(selectionViews[p].transform, false);
                }
            }
        };

        // プレーヤーが選んだカードが変わった時にどうする？
        game.OnSelectionChange += () => {
            // P2（AI）がまだカードを選んでいないか
            if (game.players[1].selection == null)
            {
                // AIにカードを選んでもらって、モデルを更新する
                game.SelectCard(1, player2.SelectCard(game.players[1].tefuda));
            }
        };

        // ターンが終わったらどうする
        game.OnTurnEnd += () =>
        {
            // 少し待つ。Unityではこうやって待つ。
            StartCoroutine(WaitAndContinue());
        };

        game.OnGameEnd += () =>
        {
            resultsView.SetWinner(game.winner);
            resultsView.gameObject.SetActive(true);
        };

        // 全ての設定が終わったので、ゲームを開始させる
        StartGame();
    }

    public void StartGame()
    {
        resultsView.gameObject.SetActive(false);
        game.Start();
    }

    // このメソッドは「待つ」動作に必要
    IEnumerator WaitAndContinue()
    {
        // これは指定された秒数を待つ指示
        yield return new WaitForSeconds(1);

        // 待った後はどうする？
        // 次のターンに行くかゲームを終了させるか
        game.CheckContinue();
    }
}
