﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CardGame; // 自分がGame.csなどで定義したnamespaceを利用する

// このクラスはAIでプレーヤーと対戦する相手を実装する
public class PlayerAI : MonoBehaviour
{
    // 自分は何番目のプレーヤーか
    public int player = 1;

    // 渡された手札からカードを選ぶ
    public int SelectCard(List<Card> hand)
    {
        // 手札が空じゃないことを確認する
        if (hand != null && hand.Count > 0)
        {
            // 一番値が高いカードの順番を探す
            int index = 0;
            int max = 0;
            for (int i = 0; i < hand.Count; i++)
            {
                if (hand[i].value > max)
                {
                    max = hand[i].value;
                    index = i;
                }
            }
            // 一番強いカードを選ぶ
            return index;
        }
        else
        {
            return 0;
        }
    }
}
