# Unity Card Game

## プログラムの構造

### モデル
これはゲームのルールなどを計算するクラス：

- Game.cs
- Player.cs
- Card.cs

![Architecture](Tutorial/Architecture.png)

### ビュー
このクラスはユーザーに情報を可視化したり、ユーザーからのインタラクションを受け取る

- CardView.cs
- DeckView.cs
- HandView.cs
- ScoreView.cs

### コントローラ
このクラスはモデルとビューをつなげる

- Controller.cs

## Unityのシーン

### Hierarchy

![Hierarchy](Tutorial/Screenshot_1.png)

### コントローラ
「Game」というオブジェクトはコントローラ

![Controller](Tutorial/Screenshot_2.png)

### AI
AIのプレーヤー用のオブジェクトも用意する。

![AI](Tutorial/Screenshot_3.png)

### UI
画面はすべてUIのオブジェクトで構成されている。

![UI](Tutorial/Screenshot_6.png)

![UI](Tutorial/Screenshot_4.png)

![UI](Tutorial/Screenshot_5.png)

![UI](Tutorial/Screenshot_7.png)

### カードプレハブ
手札のカードを生成するために1枚のカードのプレハブを作る

![Prefab](Tutorial/Screenshot_9.png)

![Prefab](Tutorial/Screenshot_10.png)

### 手札などのカードを均等に配置する
Unity標準の「Horizontal Layout Group」コンポーネントを使う

![Layout](Tutorial/Screenshot_8.png)

https://docs.unity3d.com/ja/2019.2/Manual/script-HorizontalLayoutGroup.html


### カードを選択できるようにする
カードにButtonを追加して、押されたときにCardViewのOnClickを実行する。

![Button](Tutorial/Screenshot_11.png)

![Button](Tutorial/Screenshot_12.png)
