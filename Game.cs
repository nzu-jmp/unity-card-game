﻿using System;
using System.Collections.Generic;
using System.Linq;

// 「namespace」を使うと他の人が作ったコードとクラス名など
// が重複しても大丈夫。
namespace CardGame
{
    // このクラスはゲームの「モデル」で、肝心な判定など、
    // ルールを実行し、ゲームに必要なデータを管理する。
    // ゲームの見た目や演出などと全く無縁。
    public class Game
    {
        // イベント
        public Action OnGameChange; // ゲームの状態が変わった
        public Action OnGameEnd; // ゲームが終わった
        public Action OnTurnEnd; // ターンが終わった
        public Action OnSelectionChange; // 選ばれたカードが変わった

        public List<Player> players; // プレーヤーモデル
        public List<Card> deck; // 山札モデル
        public int turn; // ターン
        public int winner; // 勝利したプレーヤー（引き分け：-1）

        // ゲームを開始させる
        public void Start()
        {
            DeckJumbi(); // 山札を準備する
            PlayersJumbi(); // プレーヤーを準備する
            turn = 0; // ターンをリセット
            winner = -1; // 引き分け状態で始まる

            // C#デハイベントにだれも登録していないと、
            // そのイベントの値がnullになる。
            // 「オブジェクトがnullでなければメソッドを実行する」という
            // 指示は以下の通り「?」を使う。
            OnGameChange?.Invoke();
        }

        // ここは山札の準備
        void DeckJumbi()
        {
            // 山札は新しいリストにする。すでに情報があったら
            // 上書きされる。
            deck = new List<Card>();

            // ここは便利な「foreach」ループでカードのスーツごとに
            // 繰り返す
            foreach(string s in  Card.Suits)
            {
                // 1つのスーツに14種類のカードがあるので、
                // 各種類繰り返して、1枚ずつ生成する。
                for(int i = 1; i < 14; i++)
                {
                    // 新しいカードを生成して、山札に追加する
                    Card c = new Card(s, i);
                    deck.Add(c);
                }
            }

            // この時点で山札をシャッフルしなければならない
            ShuffleDeck();
        }

        // 山札をシャッフルする。
        // 「フィッシャー–イェーツ」というアルゴリズムを使って、
        // リストの要素の順番をバラバラにする。
        // https://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A3%E3%83%83%E3%82%B7%E3%83%A3%E3%83%BC%E2%80%93%E3%82%A4%E3%82%A7%E3%83%BC%E3%83%84%E3%81%AE%E3%82%B7%E3%83%A3%E3%83%83%E3%83%95%E3%83%AB
        public void ShuffleDeck()
        {
            Random rng = new Random();
            int n = deck.Count;
            while (n > 1)
            {
                int k = rng.Next(n);
                n--;
                Card c = deck[k];
                deck[k] = deck[n];
                deck[n] = c;
            }
        }

        // プレーヤーにカードを配る
        public void DealCard(Player p)
        {
            // 配るカードがあるのを確認する！
            if (deck.Count > 0) 
            {
                // デッキの最初のカードを記憶する
                Card c = deck[0];
                // デッキの最初のカードをデッキから消す
                deck.RemoveAt(0);
                // 記憶したカードをプレーヤーの手札に追加
                p.tefuda.Add(c);
            }
        }

        // プレーヤーを準備する
        void PlayersJumbi()
        {
            // プレーヤーのリストを新しく生成する。すでにプレーヤー情報が
            // あれば上書きされる。
            players = new List<Player>();

            // プレーヤーを2人追加する
            players.Add(new Player());
            players.Add(new Player());

            // 各プレーヤーごと...
            foreach(Player p in players)
            {
                // カードを5枚配る
                for (int i = 0; i < 5; i++)
                {
                    DealCard(p);
                }
            }
        }

        // 新しいターンを開始させる
        public void NewTurn()
        {
            // 各プレーヤーごと...
            foreach(Player p in players)
            {
                // 選んだカードをリセットする
                p.selection = null;
                DealCard(p); // カードを1枚配る
            }

            // ターン情報を更新する
            turn++;

            // イベントを送信
            OnGameChange?.Invoke();
        }

        // カードを選ぶ
        public void SelectCard(int player, int n)
        {
            // 外部から実行されるメソッドは必ず、渡された情報が
            // おかしくないかを確認する！
            if (player < players.Count && 
                n < players[player].tefuda.Count &&
                players[player].selection == null)
            {
                // プレーヤーの手札からカードの情報を取得する
                players[player].selection = players[player].tefuda[n];
                // 手札から選んだカードを消す
                players[player].tefuda.RemoveAt(n);

                // 以下の行はLinqというC#の非常にパワフルで便利な機能を
                // 使って全てのプレーヤーがカードを選んだかを確認する。
                // 「players.Any」はリストに条件に該当する要素があるかどうかを
                // 返す。条件は以下の形で記述する。ここの「p」は自由に指定
                // する変数名で、その後に「=>」を書いて、条件を書く。
                // ここはプレーヤーのselectionがnullを確認する。
                // 全部合わせてカード選択未完了のプレーヤーがいるか。
                // 冒頭に否定の「!」を付けることで、「もしカード選択を
                // 済ませていないプレーヤーがいなかったら」という文になる。
                if (!players.Any(p => p.selection == null))
                {
                    // スコアを計算する
                    UpdateScore();
                    // ターンが終わったことを知らせる
                    OnTurnEnd?.Invoke();
                }
                else
                {
                    // 選択が変わったことを知らせる
                    OnSelectionChange?.Invoke();
                }

                // ゲームの状態が変わったことを知らせる
                OnGameChange?.Invoke();
            }
        }

        // 全てのプレーヤーのスコア計算はここ
        public void UpdateScore()
        {
            // P1のスコアを計算する
            players[0].score += CalcScore(
                players[0].selection,
                players[1].selection
            );

            // P2のスコアを計算する
            players[1].score += CalcScore(
                players[1].selection,
                players[0].selection
            );
        }

        //　1人のスコアを計算する
        public int CalcScore(Card ownCard, Card otherCard)
        {
            // 値が高い方が勝ち
            if (ownCard.value > otherCard.value)
            {
                return 1;
            }
            return 0;
        }

        // 次のターンに行くか、ゲームを終了させるか
        public void CheckContinue()
        {
            // 山札に十分にカードが残っているか？
            if (deck.Count >= players.Count)
            {
                NewTurn();
            }
            else
            {
                EndGame();
            }
        }

        // ゲームを終了させる
        public void EndGame()
        {
            // 勝者判定
            if (players[0].score > players[1].score)
            {
                winner = 0;
            }
            if (players[0].score < players[1].score)
            {
                winner = 1;
            }
            else {
                winner = -1;
            }

            // ゲームが終わったことを通知する
            OnGameEnd?.Invoke();
        }
    }
}
