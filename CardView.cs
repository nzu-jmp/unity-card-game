﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

// カードのView
public class CardView : MonoBehaviour
{
    // UnityのUIコンポーネントへの参照
    Text suitLabel;
    Text valueLabel;
    Image panel;
    GameObject back; // カードを伏せるオブジェクト

    // カードの情報
    string suit;
    int value;
    int player;
    int index;

    // イベント通知のAction
    public Action<int, int> OnSelect;

    // Start is called before the first frame update
    void Awake()
    {
        // 必要なコンポーネントを探す
        panel = GetComponent<Image>();
        back = transform.Find("Back").gameObject;
        suitLabel = transform.Find("Suit").GetComponent<Text>();
        valueLabel = transform.Find("Value").GetComponent<Text>();
    }

    // カードを伏せるかどうか
    public void SetHidden(bool val)
    {
        // オブジェクトを有効化する
        back.SetActive(val);
    }

    // カードの初期化
    public void Init(string suit, int value, int player, int index)
    {
        this.suit = suit;
        this.value = value;
        this.index = index;
        this.player = player;
        suitLabel.text = suit;
        valueLabel.text = value.ToString();
    }

    // カードがクリックされたときに実行するメソッド
    // UnityでButtonコンポーネントのOnClickと繋げる。
    public void OnClick()
    {
        // イベントを送信する
        OnSelect?.Invoke(player, index);
    }
}
