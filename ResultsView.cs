﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsView : MonoBehaviour
{
    Text label;
    
    void Awake()
    {
        label = GetComponentInChildren<Text>();
    }

    public void SetWinner(int p)
    {
        if (p == -1)
        {
            label.text = "Draw!";
        }
        else
        {
            label.text = "P" + (p + 1) + " wins!";
        }
    }
}
