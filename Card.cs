﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame
{
    public class Card
    {
        public static string[] Suits = {
            "heart",
            "diamond",
            "clubs",
            "spade"
        };

        public int value;
        public string suit;

        // これは「コンストラクタ」という特別なメソッド。
        // クラスから新しいオブジェクトを生成したときに
        // 実行される。ここで初期化を行う。
        public Card(string suit, int value)
        {
            this.suit = suit;
            this.value = value;
        }
    }
}
