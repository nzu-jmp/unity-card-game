﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame
{
    public class Player
    {
        public static string[] Zokusei = {
            "闇",
            "光"
        };

        public List<Card> tefuda;
        public Card selection;
        public int score;

        public Player()
        {
            this.tefuda = new List<Card>();
            this.selection = null;
            this.score = 0;
        }
    }
}
